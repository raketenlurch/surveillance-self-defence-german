# surveillance-self-defence-german

German translation of the Surveillance Self Defence guide written by the Electronic Frontier Foundation.

[Source](https://ssd.eff.org/en)


# How to Contribute? Wie kann ich mitmachen?

You are interested in privacy issues and are able to read and write German? Then, it would be nice to translate some portions of the original text. If you familiar with colaboration via codeberg, have a look at the [issues](https://codeberg.org/raketenlurch/surveillance-self-defence-german/issues). If you prefer not to subscribe to another platform you can contact the maintainer via [messenger](https://raketenlurch.codeberg.page/content/messenger.html) or [mastodon](https://chaos.social/@raketenlurch/105304826882888764).

## Geschlechtersensitive Sprache
Da es mir ein wichtiges Anliegen ist die Sichtbarkeit von Personengruppen zu 
erhöhen, die in der Hacking-, und Privacy-Szene unterrepräsentiert sind, würde 
ich Menschen, die sich an der Übersetztung beteiligen wollen, bitten, entweder 
neutrale Bezeichnungen zu verwenden (z.B. Studierende) oder, wo das nicht 
möglich ist, mit einem Doppelpunkt zu gendern (z.B. Mitarbeiter:innen).

Den Doppelpunkt habe ich deswegen gewählt, weil es die einzige, mir bekannte,
Form des Genderns ist, dass auch für Menschen, die Screenreader benutzen den
intendierten Effekt hat und damit barrierefrei ist.
